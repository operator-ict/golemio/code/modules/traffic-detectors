import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { TrafficDetectorsTransformation } from "#ie/TrafficDetectorsTransformation";

chai.use(chaiAsPromised);

describe("TrafficDetectorsTransformation", () => {
    let transformation: TrafficDetectorsTransformation;
    let trafficdetectors: Record<string, any>;
    let trafficdetectorsTransformed: Record<string, any>;

    beforeEach(async () => {
        transformation = new TrafficDetectorsTransformation();
        trafficdetectors = JSON.parse(fs.readFileSync(__dirname + "/data/trafficdetectors-raw.json", "utf8"));
        trafficdetectorsTransformed = JSON.parse(fs.readFileSync(__dirname + "/data/trafficdetectors-transformed.json", "utf8"));
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("TSKSTD");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should transform data correctly", async () => {
        const data = await transformation.transform(trafficdetectors);
        expect(data).to.be.deep.equal(trafficdetectorsTransformed);
    });
});
