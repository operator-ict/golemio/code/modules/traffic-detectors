import sinon, { SinonSandbox, SinonSpy } from "sinon";
import fs from "fs";
import axios from "axios";
import { config } from "@golemio/core/dist/integration-engine/config";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { DataSourceStream } from "@golemio/core/dist/integration-engine/datasources";
import { TrafficDetectorsWorker } from "#ie/TrafficDetectorsWorker";

const waitTillStreamEnds = async (stream: DataSourceStream) => {
    await new Promise((resolve) => {
        const checker = setInterval(async () => {
            if (stream.destroyed) {
                clearInterval(checker);
                resolve(null);
            }
        }, 100);
    });
};

describe("MeteosensorsWorker", () => {
    let worker: TrafficDetectorsWorker;
    let sandbox: SinonSandbox;
    let trafficdetectors: Record<string, any>;
    let trafficdetectorsTransformed: Record<string, any>;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: () => {
                    return {
                        removeAttribute: () => {
                            return;
                        },
                    };
                },
            })
        );

        sandbox.stub(axios, "post").resolves({
            data: `
                <s:Envelope>
                    <s:Body>
                        <CreateSequenceResponse>
                            <Identifier>TEST</Identifier>
                        </CreateSequenceResponse>
                    </s:Body>
                </s:Envelope>
            `,
        });

        sandbox.stub(config, "datasources").value({
            TSKSTD: {},
        });

        trafficdetectors = JSON.parse(fs.readFileSync(__dirname + "/data/trafficdetectors-raw.json", "utf8"));
        trafficdetectorsTransformed = JSON.parse(fs.readFileSync(__dirname + "/data/trafficdetectors-transformed.json", "utf8"));

        worker = new TrafficDetectorsWorker();

        const getOutputStream = async (data: any, stream: DataSourceStream) => {
            stream.push(data);
            stream.push(null);
            return stream;
        };

        const dataStream = new DataSourceStream({
            objectMode: true,
            read: () => {
                return;
            },
        });

        sandbox
            .stub(worker["dataSource"], "getOutputStream" as any)
            .callsFake(() => getOutputStream(trafficdetectors, dataStream));
        sandbox.stub(worker["transformation"], "transform").callsFake(() => Promise.resolve(trafficdetectorsTransformed));

        sandbox.stub(worker["measurementsModel"], "saveBySqlFunction");
        sandbox.stub(worker["errorsModel"], "saveBySqlFunction");
        sandbox.spy(worker["dataSource"], "getAll");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should calls the correct methods by refreshDataInDB method", async () => {
        await worker.saveNewTSKSTDDataInDB({});

        await waitTillStreamEnds(worker["dataStream"]);

        sandbox.assert.calledOnce(worker["dataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["transformation"].transform as SinonSpy);
        sandbox.assert.calledWith(worker["transformation"].transform as SinonSpy, trafficdetectors);
        sandbox.assert.calledOnce(worker["measurementsModel"].saveBySqlFunction as SinonSpy);
        sandbox.assert.calledWith(
            worker["measurementsModel"].saveBySqlFunction as SinonSpy,
            trafficdetectorsTransformed.data,
            ["detector_id", "measured_from", "measured_to", "measurement_type", "class_id"],
            false,
            null,
            null,
            trafficdetectorsTransformed.token
        );
        sandbox.assert.calledOnce(worker["errorsModel"].saveBySqlFunction as SinonSpy);
        sandbox.assert.calledWith(
            worker["errorsModel"].saveBySqlFunction as SinonSpy,
            trafficdetectorsTransformed.errors,
            ["detector_id", "error_id", "measured_at"],
            false,
            null,
            null,
            trafficdetectorsTransformed.token
        );

        sandbox.assert.callOrder(
            worker["dataSource"].getAll as SinonSpy,
            worker["transformation"].transform as SinonSpy,
            worker["measurementsModel"].saveBySqlFunction as SinonSpy,
            worker["errorsModel"].saveBySqlFunction as SinonSpy
        );
    });
});
