import { SchemaDefinition } from "@golemio/core/dist/shared/mongoose";
import Sequelize from "@golemio/core/dist/shared/sequelize";

// MSO = Mongoose SchemaObject

// INSERT or UPDATE

const outputTSKSTDMeasurementsSDMA: Sequelize.ModelAttributes<any> = {
    class_id: {
        primaryKey: true,
        type: Sequelize.INTEGER,
    },
    detector_id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    measured_from: {
        primaryKey: true,
        type: Sequelize.INTEGER,
    },
    measured_to: {
        primaryKey: true,
        type: Sequelize.INTEGER,
    },
    measurement_type: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    value: Sequelize.INTEGER,

    // Audit database fields
    create_batch_id: { type: Sequelize.BIGINT }, // ID of input batch
    created_at: { type: Sequelize.DATE }, // Time of insertion
    created_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of inserion
    update_batch_id: { type: Sequelize.BIGINT }, // ID last input batch which caused update
    updated_at: { type: Sequelize.DATE }, // Time of update
    updated_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of update
};

// only for Validator
const outputTSKSTDMeasurementsMSO: SchemaDefinition = {
    class_id: { type: Number, required: true },
    detector_id: { type: String, required: true },
    measured_from: { type: Number, required: true },
    measured_to: { type: Number, required: true },
    measurement_type: { type: String, required: true },
    value: { type: Number, required: true },
};

const outputTSKSTDErrorsSDMA: Sequelize.ModelAttributes<any> = {
    detector_id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    error_desc: Sequelize.STRING,
    error_id: {
        primaryKey: true,
        type: Sequelize.STRING,
    },
    measured_at: {
        primaryKey: true,
        type: Sequelize.BIGINT,
    },
    measured_at_iso: Sequelize.DATE,

    // Audit database fields
    create_batch_id: { type: Sequelize.BIGINT }, // ID of input batch
    created_at: { type: Sequelize.DATE }, // Time of insertion
    created_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of inserion
    update_batch_id: { type: Sequelize.BIGINT }, // ID last input batch which caused update
    updated_at: { type: Sequelize.DATE }, // Time of update
    updated_by: { type: Sequelize.STRING(150) }, // Author or name of Worker of update
};

// only for Validator
const outputTSKSTDErrorsMSO: SchemaDefinition = {
    detector_id: { type: String, required: true },
    error_desc: { type: String, required: true },
    error_id: { type: String, required: true },
    measured_at: { type: Number, required: true },
    measured_at_iso: { type: Date, required: true },
};

const forExport = {
    errors: {
        name: "TSKSTDErrors",
        outputMongooseSchemaObject: outputTSKSTDErrorsMSO,
        outputSequelizeAttributes: outputTSKSTDErrorsSDMA,
        pgTableName: "tsk_std_errors",
    },
    measurements: {
        name: "TSKSTDMeasurements",
        outputMongooseSchemaObject: outputTSKSTDMeasurementsMSO,
        outputSequelizeAttributes: outputTSKSTDMeasurementsSDMA,
        pgTableName: "tsk_std_measurements",
    },
    name: "TSKSTD",
};
export { forExport as TSKSTD };
