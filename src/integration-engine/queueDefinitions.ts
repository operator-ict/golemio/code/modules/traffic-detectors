import { config } from "@golemio/core/dist/integration-engine/config";
import { IQueueDefinition } from "@golemio/core/dist/integration-engine/queueprocessors";
import { TSKSTD } from "#sch/index";
import { TrafficDetectorsWorker } from "#ie/TrafficDetectorsWorker";

const queueDefinitions: IQueueDefinition[] = [
    {
        name: TSKSTD.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + TSKSTD.name.toLowerCase(),
        queues: [
            {
                name: "saveNewTSKSTDDataInDB",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 1 * 60 * 1000, // 1 minute
                },
                worker: TrafficDetectorsWorker,
                workerMethod: "saveNewTSKSTDDataInDB",
            },
        ],
    },
];

export { queueDefinitions };
