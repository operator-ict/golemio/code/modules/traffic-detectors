# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.0.8] - 2022-06-21

### Changed

-   Typescript version update from 4.4.4 to 4.6.4
-   Request-promise deprecated to Axios ([core#24](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/24))
